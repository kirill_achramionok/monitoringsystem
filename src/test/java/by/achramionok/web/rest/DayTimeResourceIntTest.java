package by.achramionok.web.rest;

import by.achramionok.MonitoringsystemApp;

import by.achramionok.domain.DayTime;
import by.achramionok.repository.DayTimeRepository;
import by.achramionok.service.DayTimeService;
import by.achramionok.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static by.achramionok.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DayTimeResource REST controller.
 *
 * @see DayTimeResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = MonitoringsystemApp.class)
public class DayTimeResourceIntTest {

    private static final Instant DEFAULT_DATE = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_DATE = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_TIME_SPENT_AT_WORK = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_TIME_SPENT_AT_WORK = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private DayTimeRepository dayTimeRepository;

    @Autowired
    private DayTimeService dayTimeService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDayTimeMockMvc;

    private DayTime dayTime;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DayTimeResource dayTimeResource = new DayTimeResource(dayTimeService);
        this.restDayTimeMockMvc = MockMvcBuilders.standaloneSetup(dayTimeResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DayTime createEntity(EntityManager em) {
        DayTime dayTime = new DayTime()
            .date(DEFAULT_DATE)
            .timeSpentAtWork(DEFAULT_TIME_SPENT_AT_WORK);
        return dayTime;
    }

    @Before
    public void initTest() {
        dayTime = createEntity(em);
    }

    @Test
    @Transactional
    public void createDayTime() throws Exception {
        int databaseSizeBeforeCreate = dayTimeRepository.findAll().size();

        // Create the DayTime
        restDayTimeMockMvc.perform(post("/api/day-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayTime)))
            .andExpect(status().isCreated());

        // Validate the DayTime in the database
        List<DayTime> dayTimeList = dayTimeRepository.findAll();
        assertThat(dayTimeList).hasSize(databaseSizeBeforeCreate + 1);
        DayTime testDayTime = dayTimeList.get(dayTimeList.size() - 1);
        assertThat(testDayTime.getDate()).isEqualTo(DEFAULT_DATE);
        assertThat(testDayTime.getTimeSpentAtWork()).isEqualTo(DEFAULT_TIME_SPENT_AT_WORK);
    }

    @Test
    @Transactional
    public void createDayTimeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = dayTimeRepository.findAll().size();

        // Create the DayTime with an existing ID
        dayTime.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDayTimeMockMvc.perform(post("/api/day-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayTime)))
            .andExpect(status().isBadRequest());

        // Validate the DayTime in the database
        List<DayTime> dayTimeList = dayTimeRepository.findAll();
        assertThat(dayTimeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDayTimes() throws Exception {
        // Initialize the database
        dayTimeRepository.saveAndFlush(dayTime);

        // Get all the dayTimeList
        restDayTimeMockMvc.perform(get("/api/day-times?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(dayTime.getId().intValue())))
            .andExpect(jsonPath("$.[*].date").value(hasItem(DEFAULT_DATE.toString())))
            .andExpect(jsonPath("$.[*].timeSpentAtWork").value(hasItem(DEFAULT_TIME_SPENT_AT_WORK.toString())));
    }

    @Test
    @Transactional
    public void getDayTime() throws Exception {
        // Initialize the database
        dayTimeRepository.saveAndFlush(dayTime);

        // Get the dayTime
        restDayTimeMockMvc.perform(get("/api/day-times/{id}", dayTime.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(dayTime.getId().intValue()))
            .andExpect(jsonPath("$.date").value(DEFAULT_DATE.toString()))
            .andExpect(jsonPath("$.timeSpentAtWork").value(DEFAULT_TIME_SPENT_AT_WORK.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDayTime() throws Exception {
        // Get the dayTime
        restDayTimeMockMvc.perform(get("/api/day-times/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDayTime() throws Exception {
        // Initialize the database
        dayTimeService.save(dayTime);

        int databaseSizeBeforeUpdate = dayTimeRepository.findAll().size();

        // Update the dayTime
        DayTime updatedDayTime = dayTimeRepository.findOne(dayTime.getId());
        // Disconnect from session so that the updates on updatedDayTime are not directly saved in db
        em.detach(updatedDayTime);
        updatedDayTime
            .date(UPDATED_DATE)
            .timeSpentAtWork(UPDATED_TIME_SPENT_AT_WORK);

        restDayTimeMockMvc.perform(put("/api/day-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDayTime)))
            .andExpect(status().isOk());

        // Validate the DayTime in the database
        List<DayTime> dayTimeList = dayTimeRepository.findAll();
        assertThat(dayTimeList).hasSize(databaseSizeBeforeUpdate);
        DayTime testDayTime = dayTimeList.get(dayTimeList.size() - 1);
        assertThat(testDayTime.getDate()).isEqualTo(UPDATED_DATE);
        assertThat(testDayTime.getTimeSpentAtWork()).isEqualTo(UPDATED_TIME_SPENT_AT_WORK);
    }

    @Test
    @Transactional
    public void updateNonExistingDayTime() throws Exception {
        int databaseSizeBeforeUpdate = dayTimeRepository.findAll().size();

        // Create the DayTime

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDayTimeMockMvc.perform(put("/api/day-times")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(dayTime)))
            .andExpect(status().isCreated());

        // Validate the DayTime in the database
        List<DayTime> dayTimeList = dayTimeRepository.findAll();
        assertThat(dayTimeList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteDayTime() throws Exception {
        // Initialize the database
        dayTimeService.save(dayTime);

        int databaseSizeBeforeDelete = dayTimeRepository.findAll().size();

        // Get the dayTime
        restDayTimeMockMvc.perform(delete("/api/day-times/{id}", dayTime.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DayTime> dayTimeList = dayTimeRepository.findAll();
        assertThat(dayTimeList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DayTime.class);
        DayTime dayTime1 = new DayTime();
        dayTime1.setId(1L);
        DayTime dayTime2 = new DayTime();
        dayTime2.setId(dayTime1.getId());
        assertThat(dayTime1).isEqualTo(dayTime2);
        dayTime2.setId(2L);
        assertThat(dayTime1).isNotEqualTo(dayTime2);
        dayTime1.setId(null);
        assertThat(dayTime1).isNotEqualTo(dayTime2);
    }
}
