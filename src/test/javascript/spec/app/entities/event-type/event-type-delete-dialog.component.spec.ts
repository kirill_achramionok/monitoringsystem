/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs/Observable';
import { JhiEventManager } from 'ng-jhipster';

import { MonitoringsystemTestModule } from '../../../test.module';
import { EventTypeDeleteDialogComponent } from '../../../../../../main/webapp/app/entities/event-type/event-type-delete-dialog.component';
import { EventTypeService } from '../../../../../../main/webapp/app/entities/event-type/event-type.service';

describe('Component Tests', () => {

    describe('EventType Management Delete Component', () => {
        let comp: EventTypeDeleteDialogComponent;
        let fixture: ComponentFixture<EventTypeDeleteDialogComponent>;
        let service: EventTypeService;
        let mockEventManager: any;
        let mockActiveModal: any;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MonitoringsystemTestModule],
                declarations: [EventTypeDeleteDialogComponent],
                providers: [
                    EventTypeService
                ]
            })
            .overrideTemplate(EventTypeDeleteDialogComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventTypeDeleteDialogComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventTypeService);
            mockEventManager = fixture.debugElement.injector.get(JhiEventManager);
            mockActiveModal = fixture.debugElement.injector.get(NgbActiveModal);
        });

        describe('confirmDelete', () => {
            it('Should call delete service on confirmDelete',
                inject([],
                    fakeAsync(() => {
                        // GIVEN
                        spyOn(service, 'delete').and.returnValue(Observable.of({}));

                        // WHEN
                        comp.confirmDelete(123);
                        tick();

                        // THEN
                        expect(service.delete).toHaveBeenCalledWith(123);
                        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
                        expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
                    })
                )
            );
        });
    });

});
