/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { MonitoringsystemTestModule } from '../../../test.module';
import { EventTypeDetailComponent } from '../../../../../../main/webapp/app/entities/event-type/event-type-detail.component';
import { EventTypeService } from '../../../../../../main/webapp/app/entities/event-type/event-type.service';
import { EventType } from '../../../../../../main/webapp/app/entities/event-type/event-type.model';

describe('Component Tests', () => {

    describe('EventType Management Detail Component', () => {
        let comp: EventTypeDetailComponent;
        let fixture: ComponentFixture<EventTypeDetailComponent>;
        let service: EventTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MonitoringsystemTestModule],
                declarations: [EventTypeDetailComponent],
                providers: [
                    EventTypeService
                ]
            })
            .overrideTemplate(EventTypeDetailComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventTypeDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN

                spyOn(service, 'find').and.returnValue(Observable.of(new HttpResponse({
                    body: new EventType(123)
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.find).toHaveBeenCalledWith(123);
                expect(comp.eventType).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
