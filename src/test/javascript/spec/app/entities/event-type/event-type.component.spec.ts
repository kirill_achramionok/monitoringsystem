/* tslint:disable max-line-length */
import { ComponentFixture, TestBed, async } from '@angular/core/testing';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { MonitoringsystemTestModule } from '../../../test.module';
import { EventTypeComponent } from '../../../../../../main/webapp/app/entities/event-type/event-type.component';
import { EventTypeService } from '../../../../../../main/webapp/app/entities/event-type/event-type.service';
import { EventType } from '../../../../../../main/webapp/app/entities/event-type/event-type.model';

describe('Component Tests', () => {

    describe('EventType Management Component', () => {
        let comp: EventTypeComponent;
        let fixture: ComponentFixture<EventTypeComponent>;
        let service: EventTypeService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                imports: [MonitoringsystemTestModule],
                declarations: [EventTypeComponent],
                providers: [
                    EventTypeService
                ]
            })
            .overrideTemplate(EventTypeComponent, '')
            .compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(EventTypeComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EventTypeService);
        });

        describe('OnInit', () => {
            it('Should call load all on init', () => {
                // GIVEN
                const headers = new HttpHeaders().append('link', 'link;link');
                spyOn(service, 'query').and.returnValue(Observable.of(new HttpResponse({
                    body: [new EventType(123)],
                    headers
                })));

                // WHEN
                comp.ngOnInit();

                // THEN
                expect(service.query).toHaveBeenCalled();
                expect(comp.eventTypes[0]).toEqual(jasmine.objectContaining({id: 123}));
            });
        });
    });

});
