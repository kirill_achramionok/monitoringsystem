package by.achramionok.repository;

import by.achramionok.domain.DayTime;
import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.*;

import java.time.Instant;
import java.util.List;

/**
 * Spring Data JPA repository for the DayTime entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DayTimeRepository extends JpaRepository<DayTime, Long> {

    @Query("select day_time from DayTime day_time where day_time.user.login = ?#{principal.username}")
    List<DayTime> findByUserIsCurrentUser();
    DayTime findFirstByUserId(Long userId);
    List<DayTime> findAllByUserIdAndDateBetween(Long userId, Instant startDate, Instant endDate);
}
