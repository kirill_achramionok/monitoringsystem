package by.achramionok.repository;

import by.achramionok.domain.Event;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.util.List;


/**
 * Spring Data JPA repository for the Event entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EventRepository extends JpaRepository<Event, Long> {
    Event findEventByUserId(Long userId);

    List<Event> findAllByUserIdAndEventdateBetweenOrderByEventdateAsc(Long userId, Instant dateFrom, Instant dateTo);

    List<Event> findAllByUserId(Long userId);

    Event findFirstByUserIdOrderByEventdateDesc(Long userId);
}
