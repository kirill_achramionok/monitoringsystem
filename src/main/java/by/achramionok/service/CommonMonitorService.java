package by.achramionok.service;

import by.achramionok.domain.Event;
import by.achramionok.domain.User;
import by.achramionok.repository.DayTimeRepository;
import by.achramionok.repository.EventRepository;
import by.achramionok.repository.EventTypeRepository;
import by.achramionok.repository.UserRepository;
import by.achramionok.service.dto.CommonMonitorDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
@Service
@Transactional
public class CommonMonitorService {

    private final Logger log = LoggerFactory.getLogger(CommonMonitorService.class);

    private final UserRepository userRepository;

    private final EventRepository eventRepository;

    private final DayTimeRepository dayTimeRepository;

    public CommonMonitorService(UserRepository userRepository,
                                EventRepository eventRepository,
                                DayTimeRepository dayTimeRepository) {
        this.userRepository = userRepository;
        this.eventRepository = eventRepository;
        this.dayTimeRepository = dayTimeRepository;
    }

    private Instant countTimeSpentAtWork(long userId) {
        return null;
    }

    public List<CommonMonitorDTO> getCommonMonitorData() {
        List<CommonMonitorDTO> commonMonitorDTOList = new ArrayList<>();
        for (User user : userRepository.findAll()) {
            Event event = eventRepository.findFirstByUserIdOrderByEventdateDesc(user.getId());
            if (event != null) {
                String status;
                if (event.getEventType().getEventname().equals("Enter")) {
                    status = "Attend";
                } else {
                    status = "Absent";
                }
                commonMonitorDTOList.add(new CommonMonitorDTO(user.getId(),
                    user.getFirstName() + " " + user.getLastName(),
                    status,
                    event.getEventdate(), dayTimeRepository.findFirstByUserId(user.getId()).getTimeSpentAtWork()));
            }
        }
        return commonMonitorDTOList;
    }
}
