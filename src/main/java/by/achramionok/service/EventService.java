package by.achramionok.service;

import by.achramionok.domain.Event;
import by.achramionok.repository.EventRepository;
import by.achramionok.service.dto.EventDTO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
@Service
public class EventService {
    private final EventRepository eventRepository;

    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public List<EventDTO> getEventDTO(Long userId) {
        List<EventDTO> eventDTOS = new ArrayList<>();
        List<Event> events = eventRepository.findAllByUserId(userId);
        for (Event event :
            events) {
            eventDTOS.add(new EventDTO(event.getId(),
                event.getEventdate().getEpochSecond(),
                event.getEventType().getId(),
                event.getUser().getId())
            );
        }
        return eventDTOS;
    }

}
