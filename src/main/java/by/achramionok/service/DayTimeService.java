package by.achramionok.service;

import by.achramionok.domain.DayTime;
import by.achramionok.repository.DayTimeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Service Implementation for managing DayTime.
 */
@Service
@Transactional
public class DayTimeService {

    private final Logger log = LoggerFactory.getLogger(DayTimeService.class);

    private final DayTimeRepository dayTimeRepository;

    public DayTimeService(DayTimeRepository dayTimeRepository) {
        this.dayTimeRepository = dayTimeRepository;
    }

    /**
     * Save a dayTime.
     *
     * @param dayTime the entity to save
     * @return the persisted entity
     */
    public DayTime save(DayTime dayTime) {
        log.debug("Request to save DayTime : {}", dayTime);
        return dayTimeRepository.save(dayTime);
    }

    /**
     * Get all the dayTimes.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<DayTime> findAll() {
        log.debug("Request to get all DayTimes");
        return dayTimeRepository.findAll();
    }

    /**
     * Get one dayTime by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public DayTime findOne(Long id) {
        log.debug("Request to get DayTime : {}", id);
        return dayTimeRepository.findOne(id);
    }

    /**
     * Delete the dayTime by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete DayTime : {}", id);
        dayTimeRepository.delete(id);
    }
}
