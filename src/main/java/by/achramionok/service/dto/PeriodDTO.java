package by.achramionok.service.dto;

import java.time.Instant;
import java.time.LocalTime;

public class PeriodDTO {
    private Long userId;
    private Instant dateFrom;
    private Instant dateTo;

    public PeriodDTO() {
    }

    public PeriodDTO(Long userId, Instant dateFrom, Instant dateTo) {
        this.userId = userId;
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Instant getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(Instant dateFrom) {
        this.dateFrom = dateFrom;
    }

    public Instant getDateTo() {
        return dateTo;
    }

    public void setDateTo(Instant dateTo) {
        this.dateTo = dateTo;
    }
}
