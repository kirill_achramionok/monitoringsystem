package by.achramionok.service.dto;

import java.sql.Timestamp;
import java.time.Instant;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
public class CommonMonitorDTO {
    private Long userId;
    private String concatName;
    private String status;
    private Instant lastTimeStateChanges;
    private Instant timeSpentAtWork;

    public CommonMonitorDTO(Long userId,
                            String concatName,
                            String status,
                            Instant lastTimeStateChanges,
                            Instant timeSpentAtWork) {
        this.userId = userId;
        this.concatName = concatName;
        this.status = status;
        this.lastTimeStateChanges = lastTimeStateChanges;
        this.timeSpentAtWork = timeSpentAtWork;
    }

    public Instant getTimeSpentAtWork() {
        return timeSpentAtWork;
    }

    public void setTimeSpentAtWork(Instant timeSpentAtWork) {
        this.timeSpentAtWork = timeSpentAtWork;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getConcatName() {
        return concatName;
    }

    public void setConcatName(String concatName) {
        this.concatName = concatName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Instant getLastTimeStateChanges() {
        return lastTimeStateChanges;
    }

    public void setLastTimeStateChanges(Instant lastTimeStateChanges) {
        this.lastTimeStateChanges = lastTimeStateChanges;
    }
}
