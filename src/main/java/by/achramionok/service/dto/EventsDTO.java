package by.achramionok.service.dto;

import java.time.Instant;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
public class EventsDTO {
    private Instant date;
    private Instant eventDate;
    private String eventType;

    public EventsDTO(Instant date, Instant eventDate, String eventType) {
        this.date = date;
        this.eventDate = eventDate;
        this.eventType = eventType;
    }

    public Instant getDate() {
        return date;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
