package by.achramionok.service.dto;

import java.time.Instant;
import java.time.LocalTime;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
public class DetailMonitorDTO {
    private Instant day;
    private LocalTime timeSpentAtWork;
    private LocalTime timeAtWorkNormal;
    private Instant eventDate;
    private String eventType;

    public DetailMonitorDTO() {
    }

    public DetailMonitorDTO(Instant day, LocalTime timeSpentAtWork, LocalTime timeAtWorkNormal, Instant eventDate, String eventType) {
        this.day = day;
        this.timeSpentAtWork = timeSpentAtWork;
        this.timeAtWorkNormal = timeAtWorkNormal;
        this.eventDate = eventDate;
        this.eventType = eventType;
    }

    public Instant getDay() {
        return day;
    }

    public void setDay(Instant day) {
        this.day = day;
    }

    public LocalTime getTimeSpentAtWork() {
        return timeSpentAtWork;
    }

    public void setTimeSpentAtWork(LocalTime timeSpentAtWork) {
        this.timeSpentAtWork = timeSpentAtWork;
    }

    public LocalTime getTimeAtWorkNormal() {
        return timeAtWorkNormal;
    }

    public void setTimeAtWorkNormal(LocalTime timeAtWorkNormal) {
        this.timeAtWorkNormal = timeAtWorkNormal;
    }

    public Instant getEventDate() {
        return eventDate;
    }

    public void setEventDate(Instant eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }
}
