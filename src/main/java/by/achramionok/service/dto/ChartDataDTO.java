package by.achramionok.service.dto;

import java.util.List;

public class ChartDataDTO {
    private List<String> labels;
    private List<ChartDataSetDTO> datasets;

    public ChartDataDTO(List<String> labels, List<ChartDataSetDTO> datasets) {
        this.labels = labels;
        this.datasets = datasets;
    }

    public ChartDataDTO() {
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public List<ChartDataSetDTO> getDatasets() {
        return datasets;
    }

    public void setDatasets(List<ChartDataSetDTO> datasets) {
        this.datasets = datasets;
    }
}
