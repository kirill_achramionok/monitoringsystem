package by.achramionok.service.dto;

import java.time.Instant;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
public class EventDTO {
    private Long id;
    private Long eventdate;
    private Long eventType;
    private Long userId;

    public EventDTO(Long id, Long eventdate, Long eventType, Long userId) {
        this.id = id;
        this.eventdate = eventdate;
        this.eventType = eventType;
        this.userId = userId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getEventdate() {
        return eventdate;
    }

    public void setEventdate(Long eventdate) {
        this.eventdate = eventdate;
    }

    public Long getEventType() {
        return eventType;
    }

    public void setEventType(Long eventType) {
        this.eventType = eventType;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
