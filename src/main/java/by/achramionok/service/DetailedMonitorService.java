package by.achramionok.service;

import by.achramionok.domain.Event;
import by.achramionok.domain.EventType;
import by.achramionok.domain.User;
import by.achramionok.repository.EventRepository;
import by.achramionok.repository.EventTypeRepository;
import by.achramionok.repository.UserRepository;
import by.achramionok.service.dto.ChartDataDTO;
import by.achramionok.service.dto.ChartDataSetDTO;
import by.achramionok.service.dto.DetailMonitorDTO;
import by.achramionok.service.dto.EventsDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.sql.DataSource;
import java.sql.*;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
@Service
@Transactional
public class DetailedMonitorService {
    private final Logger log = LoggerFactory.getLogger(CommonMonitorService.class);
    private static String [] labels = {"8:00 - 8:30","8:31 - 9:00","9:01 - 9:30","9:31 - 10:00","10:01 - 10:30","10:31 - 11:00","11:01 - 11:30","11:31 - 12:00"};
    private final EventRepository eventRepository;

    private final EventTypeRepository eventTypeRepository;

    @Autowired
    private DataSource dataSource;

    private final UserRepository userRepository;

    public DetailedMonitorService(EventRepository eventRepository, EventTypeRepository eventTypeRepository, UserRepository userRepository) {
        this.eventRepository = eventRepository;
        this.eventTypeRepository = eventTypeRepository;
        this.userRepository = userRepository;
    }

    public List<EventsDTO> getEventsByUserId(Long userId) {
        List<EventsDTO> eventsDTOList = new ArrayList<>();
        List<Event> events = eventRepository.findAllByUserId(userId);
        for (Event event : events) {

        }
        return eventsDTOList;
    }

    public List<DetailMonitorDTO> getTimeSpentAtWorkByUserId(Long userId, Instant dateFrom, Instant dateTo) {
        List<DetailMonitorDTO> detailMonitorDTOList = new ArrayList<>();
        long days = ChronoUnit.DAYS.between(dateFrom, dateTo);
        for (int i = 0; i < days; i++) {
            List<Event> events = eventRepository.findAllByUserIdAndEventdateBetweenOrderByEventdateAsc(
                userId,
                dateFrom,
                dateFrom = dateFrom.plus(1, ChronoUnit.DAYS).minusSeconds(1)
            );
            LocalTime time = countTimePerDay(events);
            for (Event event : events
                ) {
                detailMonitorDTOList.add(new DetailMonitorDTO(
                    dateFrom,
                    time,
                    LocalTime.of(8, 0, 0),
                    event.getEventdate(),
                    event.getEventType().getEventname())
                );
            }
        }
        return detailMonitorDTOList;
    }

    private List<Event> getCountOfFirstEntersPerTime(Long userId, Long eventTypeId) throws SQLException {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("UTC"));
        List<Event> events = new ArrayList<>();
        EventType eventType = eventTypeRepository.findById(eventTypeId);
        User user = userRepository.findFirstById(userId);
        Connection connection = null;
        LocalDate firstDayOfCurrentMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfMonth());
        LocalDate firstDayOfNextMonth = LocalDate.now().with(TemporalAdjusters.firstDayOfNextMonth());
            connection = dataSource.getConnection();
            String sql = "select id, event.eventdate, event_type_id, event.user_id from event inner join (select user_id, DATE(eventdate) as day_event, " +
                "MIN (eventdate) as first_event " +
                "From event where event_type_id = ? and user_id = ? and eventdate BETWEEN ? and ? " +
                "GROUP BY user_id, DATE(eventdate)) fe " +
                "ON event.user_id = fe.user_id " +
                "And event.eventdate = fe.first_event";
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setLong(1, eventTypeId);
            preparedStatement.setLong(2, userId);
            preparedStatement.setDate(3, Date.valueOf(firstDayOfCurrentMonth));
            preparedStatement.setDate(4, Date.valueOf(firstDayOfNextMonth));
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()){
                events.add(new Event(resultSet.getLong(1),
                    Instant.from(formatter.parse(resultSet.getString(2))),
                    eventType,
                    user
                    ));
            }
            resultSet.close();
            preparedStatement.close();
            connection.close();


        return events;
    }

    public ChartDataDTO getCountOfFirstEntriesPerMonth(Long userId, Long eventTypeId) throws SQLException{
        ChartDataDTO chartDataDTO = null;
        List<Integer> data = new ArrayList<Integer>(Arrays.asList(0, 0, 0, 0, 0, 0, 0, 0));
        List<Event> events = getCountOfFirstEntersPerTime(userId, eventTypeId);
        for (Event event: events
             ) {
            LocalDateTime currentTime = LocalDateTime.ofInstant(event.getEventdate(), ZoneId.of("UTC"));
            LocalTime time = LocalTime.of(currentTime.getHour(), currentTime.getMinute(),currentTime.getSecond());
            switch (time.toSecondOfDay()/1800){
                case 16: data.set(0, data.get(0) + 1);break;
                case 17: data.set(1, data.get(1) + 1);break;
                case 18: data.set(2, data.get(2) + 1);break;
                case 19: data.set(3, data.get(3) + 1);break;
                case 20: data.set(4, data.get(4) + 1);break;
                case 21: data.set(5, data.get(5) + 1);break;
                case 22: data.set(6, data.get(6) + 1);break;
                case 23: data.set(7, data.get(7) + 1);break;
            }
        }

        chartDataDTO = new ChartDataDTO(Arrays.asList(labels), Arrays.asList(new ChartDataSetDTO("Graph of entries", data, false, "#4bc0c0")));
        return chartDataDTO;
    }

    private LocalTime countTimePerDay(List<Event> events) {
        LocalTime time = LocalTime.of(0, 0, 0);

        Event temp = null;
        for (Event event : events
            ) {
            if (event.getEventType().getEventname().equals("Enter")) {
                if (events.size() != events.indexOf(event) + 1 && (temp = events.get(events.indexOf(event) + 1)).getEventType().getEventname().equals("Exit")) {
                    time = time.plus(ChronoUnit.SECONDS.between(event.getEventdate(), temp.getEventdate()), ChronoUnit.SECONDS);
                }
            }
        }
        return time;
    }
}
