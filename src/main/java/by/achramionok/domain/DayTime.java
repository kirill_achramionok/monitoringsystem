package by.achramionok.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A DayTime.
 */
@Entity
@Table(name = "day_time")
public class DayTime implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_date")
    private Instant date;

    @Column(name = "time_spent_at_work")
    private Instant timeSpentAtWork;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getDate() {
        return date;
    }

    public DayTime date(Instant date) {
        this.date = date;
        return this;
    }

    public void setDate(Instant date) {
        this.date = date;
    }

    public Instant getTimeSpentAtWork() {
        return timeSpentAtWork;
    }

    public DayTime timeSpentAtWork(Instant timeSpentAtWork) {
        this.timeSpentAtWork = timeSpentAtWork;
        return this;
    }

    public void setTimeSpentAtWork(Instant timeSpentAtWork) {
        this.timeSpentAtWork = timeSpentAtWork;
    }

    public User getUser() {
        return user;
    }

    public DayTime user(User user) {
        this.user = user;
        return this;
    }

    public void setUser(User user) {
        this.user = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DayTime dayTime = (DayTime) o;
        if (dayTime.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), dayTime.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DayTime{" +
            "id=" + getId() +
            ", date='" + getDate() + "'" +
            ", timeSpentAtWork='" + getTimeSpentAtWork() + "'" +
            "}";
    }
}
