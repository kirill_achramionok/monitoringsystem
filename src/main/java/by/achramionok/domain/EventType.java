package by.achramionok.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A EventType.
 */
@Entity
@Table(name = "event_type")
public class EventType implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "eventname")
    private String eventname;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEventname() {
        return eventname;
    }

    public EventType eventname(String eventname) {
        this.eventname = eventname;
        return this;
    }

    public void setEventname(String eventname) {
        this.eventname = eventname;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        EventType eventType = (EventType) o;
        if (eventType.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), eventType.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "EventType{" +
            "id=" + getId() +
            ", eventname='" + getEventname() + "'" +
            "}";
    }
}
