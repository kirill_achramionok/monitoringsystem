package by.achramionok.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * A Event.
 */
@Entity
@Table(name = "event")
public class Event implements Serializable {

    private static final long serialVersionUID = 1L;

    public Event() {
    }

    public Event(Long id, Instant eventdate, EventType eventType, User user) {
        this.eventdate = eventdate;
        this.eventType = eventType;
        this.user = user;
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "eventdate")
    private Instant eventdate;

    @ManyToOne
    private EventType eventType;

    @ManyToOne
    private User user;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getEventdate() {
        return eventdate;
    }

    public Event eventdate(Instant eventdate) {
        this.eventdate = eventdate;
        return this;
    }

    public Event eventUser(User user){
        this.user = user;
        return this;
    }

    public void setEventdate(Instant eventdate) {
        this.eventdate = eventdate;
    }

    public EventType getEventType() {
        return eventType;
    }

    public Event eventType(EventType eventType) {
        this.eventType = eventType;
        return this;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Event event = (Event) o;
        if (event.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), event.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Event{" +
            "id=" + getId() +
            ", eventdate='" + getEventdate() + "'" +
            ", user='" + getUser().getFirstName() + "'" +
            "}";
    }
}
