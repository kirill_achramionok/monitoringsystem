package by.achramionok.web.rest;

import by.achramionok.service.CommonMonitorService;
import by.achramionok.service.dto.CommonMonitorDTO;
import com.codahale.metrics.annotation.Timed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author Kirill.A
 * Created 05-02-2018
 */
@RestController
@RequestMapping("/api")
public class CommonMonitorResource {
    private final Logger log = LoggerFactory.getLogger(CommonMonitorResource.class);

    private final CommonMonitorService commonMonitorService;

    public CommonMonitorResource(CommonMonitorService commonMonitorService) {
        this.commonMonitorService = commonMonitorService;
    }

    @GetMapping("/monitor/common")
    @Timed
    public ResponseEntity<List<CommonMonitorDTO>> getCommonMonitorData(){
        return new ResponseEntity<List<CommonMonitorDTO>>(commonMonitorService.getCommonMonitorData(),
            HttpStatus.OK);
    }


}
