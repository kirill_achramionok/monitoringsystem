package by.achramionok.web.rest;

import by.achramionok.domain.Event;
import by.achramionok.service.DetailedMonitorService;
import by.achramionok.service.dto.ChartDataDTO;
import by.achramionok.service.dto.DetailMonitorDTO;
import by.achramionok.service.dto.PeriodDTO;
import com.codahale.metrics.annotation.Timed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping("/api")
public class DetailedMonitorResource {
    private final DetailedMonitorService detailedMonitorService;

    public DetailedMonitorResource(DetailedMonitorService detailedMonitorService) {
        this.detailedMonitorService = detailedMonitorService;
    }

    @PostMapping("monitor/detailed")
    @Timed
    public ResponseEntity<List<DetailMonitorDTO>> getEventFromToByUserId(@RequestBody PeriodDTO periodDTO) {
        return new ResponseEntity<List<DetailMonitorDTO>>(
            detailedMonitorService.getTimeSpentAtWorkByUserId(
                periodDTO.getUserId(),
                periodDTO.getDateFrom(),
                periodDTO.getDateTo()),
            HttpStatus.OK);
    }

    @GetMapping("monitor/detailed/chart-enter/{userId}")
    @Timed
    public ResponseEntity<ChartDataDTO> getChartEnterData(@PathVariable Long userId){
        ChartDataDTO chartData = null;
        try {
            chartData = detailedMonitorService.getCountOfFirstEntriesPerMonth(userId, 1301L);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<ChartDataDTO>(chartData, HttpStatus.OK);
    }
}
