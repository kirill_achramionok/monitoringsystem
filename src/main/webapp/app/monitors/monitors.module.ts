import {NgModule, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonMonitorModule} from './common-monitor/common-monitor.module';
import {DetailedMonitorModule} from './detailed-monitor/detailed-monitor.module';

@NgModule({
    imports: [
        CommonMonitorModule,
        DetailedMonitorModule
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonitorsModule {
}
