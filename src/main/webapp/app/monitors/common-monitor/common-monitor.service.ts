import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {JhiDateUtils} from 'ng-jhipster';
import {CommonMonitorDto} from './common-monitor-dto';
import {Observable} from 'rxjs/Observable';
import {createRequestOption} from '../../shared';
import {SERVER_API_URL} from '../../app.constants';

export type EntityResponseType = HttpResponse<CommonMonitorDto>;

@Injectable()
export class CommonMonitorService {

    private resourceUrl = SERVER_API_URL + 'api/monitor/common';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

    query(req?: any): Observable<HttpResponse<CommonMonitorDto[]>> {
        const options = createRequestOption(req);
        return this.http.get<CommonMonitorDto[]>(this.resourceUrl, {params: options, observe: 'response'})
            .map((res: HttpResponse<CommonMonitorDto[]>) => this.convertArrayResponse(res));
    }

    private convertArrayResponse(res: HttpResponse<CommonMonitorDto[]>): HttpResponse<CommonMonitorDto[]> {
        const jsonResponse: CommonMonitorDto[] = res.body;
        const body: CommonMonitorDto[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    private convertItemFromServer(commonMonitorDTO: CommonMonitorDto): CommonMonitorDto {
        const copy: CommonMonitorDto = Object.assign({}, commonMonitorDTO);
        copy.lastTimeStateChanges = this.dateUtils
            .convertDateTimeFromServer(commonMonitorDTO.lastTimeStateChanges);
        return copy;
    }

}
