import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MonitoringsystemSharedModule} from '../../shared';
import {commonMonitorRoute} from './common-monitor.route';
import {ToolbarModule} from 'primeng/toolbar';
import {CommonMonitorComponent} from './common-monitor.component';
import {CommonMonitorService} from './common-monitor.service';
import {DataTableModule} from 'primeng/datatable';
import {SharedModule} from 'primeng/shared';

const ENTITY_STATES = [
    ...commonMonitorRoute,
];

@NgModule({
    imports: [
        MonitoringsystemSharedModule,
        ToolbarModule,
        DataTableModule,
        SharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        CommonMonitorComponent
    ],
    entryComponents: [
        CommonMonitorComponent
    ],
    providers: [
        CommonMonitorService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CommonMonitorModule {
}
