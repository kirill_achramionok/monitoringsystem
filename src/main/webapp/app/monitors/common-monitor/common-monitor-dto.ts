export class CommonMonitorDto {
    constructor(public userId?: number,
                public concatName?: string,
                public status?: string,
                public lastTimeStateChanges?: any,
                public timeSpentAtWork?: any) {
    }
}
