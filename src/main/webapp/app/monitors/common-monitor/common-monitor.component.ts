import {Component, OnInit} from '@angular/core';
import {CommonMonitorDto} from './common-monitor-dto';
import {CommonMonitorService} from './common-monitor.service';
import {HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {JhiAlertService, JhiEventManager} from 'ng-jhipster';
import {Principal} from '../../shared';
import {Subscription} from 'rxjs/Subscription';

@Component({
    selector: 'jhi-common-monitor',
    templateUrl: './common-monitor.component.html',
    styles: []
})
export class CommonMonitorComponent implements OnInit {
    commonMonitorDetails: CommonMonitorDto[];
    eventSubscriber: Subscription;
    currentAccount: any;

    constructor(private commonMonitorService: CommonMonitorService,
                private jhiAlertService: JhiAlertService,
                private eventManager: JhiEventManager,
                private principal: Principal) {
    }

    loadAll() {
        this.commonMonitorService.query().subscribe(
            (res: HttpResponse<CommonMonitorDto[]>) => {
                this.commonMonitorDetails = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }

    registerChangeInEvents() {
        this.eventSubscriber = this.eventManager.subscribe('monitorListModification', (response) => this.loadAll());
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEvents();
    }

}
