import { Routes} from '@angular/router';
import {UserRouteAccessService} from '../../shared';
import {CommonMonitorComponent} from './common-monitor.component';
export const commonMonitorRoute: Routes = [
    {
        path: 'common-monitor',
        component: CommonMonitorComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Common Monitor'
        },
        canActivate: [UserRouteAccessService]
    },
];
