export class ChartDataDto {
    constructor(public labels?: any,
                public datasets?: any) {
    }
}
