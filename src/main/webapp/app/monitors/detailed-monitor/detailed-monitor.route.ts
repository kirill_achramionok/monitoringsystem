import { Routes} from '@angular/router';
import {UserRouteAccessService} from '../../shared';
import {DetailedMonitorComponent} from './detailed-monitor.component';

export const detailedMonitorRoute: Routes = [
    {
        path: 'detailed-monitor',
        component: DetailedMonitorComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Detailed Monitor'
        },
        canActivate: [UserRouteAccessService]
    },
    {
        path: 'detailed-monitor/:id',
        component: DetailedMonitorComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'Detailed Monitor'
        },
        canActivate: [UserRouteAccessService]
    },
];
