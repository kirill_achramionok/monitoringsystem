import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {JhiDateUtils} from 'ng-jhipster';
import {DetailedMonitorDto} from './detailed-monitor-dto';
import {Observable} from 'rxjs/Observable';
import {createRequestOption} from '../../shared';
import {SERVER_API_URL} from '../../app.constants';
import {PeriodDto} from './period-dto';
import {ChartDataDto} from './chart-data-dto';

@Injectable()
export class DetailedMonitorService {
    private resourceUrl = SERVER_API_URL + 'api/monitor/detailed';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) {
    }

    query(periodDTO: PeriodDto, req?: any): Observable<HttpResponse<DetailedMonitorDto[]>> {
        const options = createRequestOption(req);
        const copy = this.convert(periodDTO);
        return this.http.post<DetailedMonitorDto[]>(this.resourceUrl, copy, {params: options, observe: 'response'})
            .map((res: HttpResponse<DetailedMonitorDto[]>) => this.convertArrayResponse(res));
    }

    getChartData(userId: number, req?: any): Observable<HttpResponse<ChartDataDto>> {
        const options = createRequestOption(req);
        return this.http.get<ChartDataDto>(this.resourceUrl + '/chart-enter/' + userId, {params: options, observe: 'response'})
            .map((res: HttpResponse<ChartDataDto> ) => res);
    }

    private convertArrayResponse(res: HttpResponse<DetailedMonitorDto[]>): HttpResponse<DetailedMonitorDto[]> {
        const jsonResponse: DetailedMonitorDto[] = res.body;
        const body: DetailedMonitorDto[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    private convertItemFromServer(commonMonitorDTO: DetailedMonitorDto): DetailedMonitorDto {
        const copy: DetailedMonitorDto = Object.assign({}, commonMonitorDTO);
        copy.eventDate = this.dateUtils
            .convertDateTimeFromServer(commonMonitorDTO.eventDate);
        return copy;
    }

    private convert(period: PeriodDto): PeriodDto {
        const copy: PeriodDto = Object.assign({}, period);
        // copy.dateFrom = this.dateUtils.toDate(period.dateFrom);
        // copy.dateTo = this.dateUtils.toDate(period.dateTo);
        return copy;
    }

}
