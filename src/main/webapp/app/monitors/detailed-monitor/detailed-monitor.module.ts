import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {MonitoringsystemSharedModule} from '../../shared';
import {DetailedMonitorComponent} from './detailed-monitor.component';
import {detailedMonitorRoute} from './detailed-monitor.route';
import {ButtonModule} from 'primeng/button';
import {DataTableModule} from 'primeng/datatable';
import {SharedModule} from 'primeng/shared';
import {TableModule} from 'primeng/table';
import { AutoCompleteModule, ScheduleModule, DialogModule, CalendarModule, DropdownModule } from 'primeng/primeng';
import * as jQuery from 'jquery';
import {DetailedMonitorService} from './detailed-monitor.service';
import {ChartModule} from 'primeng/chart';
(window as any).jQuery = (window as any).$ = jQuery;
const ENTITY_STATES = [
    ...detailedMonitorRoute,
];

@NgModule({
    imports: [
        MonitoringsystemSharedModule,
        ButtonModule,
        SharedModule,
        DataTableModule,
        TableModule,
        AutoCompleteModule,
        ScheduleModule,
        DialogModule,
        CalendarModule,
        DropdownModule,
        ChartModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        DetailedMonitorComponent
    ],
    entryComponents: [
        DetailedMonitorComponent
    ],
    providers: [
        DetailedMonitorService
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DetailedMonitorModule {
}
