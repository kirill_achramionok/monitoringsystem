import {AfterContentInit, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {User, UserService} from '../../shared';
import {ActivatedRoute} from '@angular/router';
import 'fullcalendar';
import {DetailedMonitorDto} from './detailed-monitor-dto';
import {DetailedMonitorService} from './detailed-monitor.service';
import {PeriodDto} from './period-dto';

@Component({
    selector: 'jhi-detailed-monitor',
    templateUrl: './detailed-monitor.component.html',
    styles: []
})
export class DetailedMonitorComponent implements OnInit, OnDestroy {
    user: User;
    chartData: any;
    dateFrom: any;
    dateTo: any;
    private subscription: Subscription;
    events: DetailedMonitorDto[];

    constructor(private userService: UserService,
                private route: ActivatedRoute,
                private detailedMonitorService: DetailedMonitorService) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params.hasOwnProperty('login')) {
                console.log('login');
                this.load(params['login']);
            } else {
                console.log('id');
                this.loadById(params['id']);
            }
        });
        // this.chartData = {
        //     labels: ['10:00-10:30', '10:30-11:00', '11:00-11:30', '11:30-12:00', '12:00-12:30', '12:30-13:00'],
        //     datasets: [
        //         {
        //             label: 'First Dataset',
        //             data: [3, 5, 2, 1, 1, 0, 0],
        //             fill: false,
        //             borderColor: '#4bc0c0'
        //         }
        //     ]
        // };
    }

    load(login) {
        this.userService.find(login).subscribe((response) => {
            this.user = response.body;
        });
    }

    loadChartData() {
        this.detailedMonitorService.getChartData(this.user.id)
            .subscribe( (response) => {
                console.log(response.body);
                this.chartData = response.body;
            });
    }

    loadById(id) {
        this.userService.findById(id).subscribe((response) => {
            this.user = response.body;
        });
    }

    onSort() {

    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

    calculate() {
        this.detailedMonitorService.query(new PeriodDto(this.user.id, this.dateFrom, this.dateTo)).subscribe((response) => {
            this.events = response.body;
        });
    }

}
