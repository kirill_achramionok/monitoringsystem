export class ChartDataSetDto {
    constructor(public label?: any,
                public data?: any,
                public fill?: any,
                public borderColor?: any) {
    }
}
