export class DetailedMonitorDto{
    constructor(public day?:any,
                public timeSpentAtWork?:any,
                public timeAtWorkNormal?:any,
                public eventDate?:any,
                public eventType?:String){

    }
}
