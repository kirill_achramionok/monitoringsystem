import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs/Subscription';

import {User, UserService} from '../../shared';

@Component({
    selector: 'jhi-user-mgmt-detail',
    templateUrl: './user-management-detail.component.html'
})
export class UserMgmtDetailComponent implements OnInit, OnDestroy {

    user: User;
    private subscription: Subscription;

    constructor(private userService: UserService,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            if (params.hasOwnProperty('login')) {
                console.log('login');
                this.load(params['login']);
            } else {
                console.log('id');
                this.loadById(params['id']);
            }
        });
    }

    load(login) {
        this.userService.find(login).subscribe((response) => {
            this.user = response.body;
        });
    }

    loadById(id) {
        this.userService.findById(id).subscribe((response) => {
            this.user = response.body;
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
