import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { JhiDateUtils } from 'ng-jhipster';

import { DayTime } from './day-time.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<DayTime>;

@Injectable()
export class DayTimeService {

    private resourceUrl =  SERVER_API_URL + 'api/day-times';

    constructor(private http: HttpClient, private dateUtils: JhiDateUtils) { }

    create(dayTime: DayTime): Observable<EntityResponseType> {
        const copy = this.convert(dayTime);
        return this.http.post<DayTime>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(dayTime: DayTime): Observable<EntityResponseType> {
        const copy = this.convert(dayTime);
        return this.http.put<DayTime>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<DayTime>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<DayTime[]>> {
        const options = createRequestOption(req);
        return this.http.get<DayTime[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<DayTime[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: DayTime = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<DayTime[]>): HttpResponse<DayTime[]> {
        const jsonResponse: DayTime[] = res.body;
        const body: DayTime[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to DayTime.
     */
    private convertItemFromServer(dayTime: DayTime): DayTime {
        const copy: DayTime = Object.assign({}, dayTime);
        copy.date = this.dateUtils
            .convertDateTimeFromServer(dayTime.date);
        copy.timeSpentAtWork = this.dateUtils
            .convertDateTimeFromServer(dayTime.timeSpentAtWork);
        return copy;
    }

    /**
     * Convert a DayTime to a JSON which can be sent to the server.
     */
    private convert(dayTime: DayTime): DayTime {
        const copy: DayTime = Object.assign({}, dayTime);

        copy.date = this.dateUtils.toDate(dayTime.date);

        copy.timeSpentAtWork = this.dateUtils.toDate(dayTime.timeSpentAtWork);
        return copy;
    }
}
