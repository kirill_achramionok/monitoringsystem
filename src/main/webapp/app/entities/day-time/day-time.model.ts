import { BaseEntity, User } from './../../shared';

export class DayTime implements BaseEntity {
    constructor(
        public id?: number,
        public date?: any,
        public timeSpentAtWork?: any,
        public user?: User,
    ) {
    }
}
