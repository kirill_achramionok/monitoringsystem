import { BaseEntity } from './../../shared';

export class Event implements BaseEntity {
    constructor(
        public id?: number,
        public eventdate?: any,
        public eventType?: BaseEntity,
        public user?: BaseEntity,
    ) {
    }
}
