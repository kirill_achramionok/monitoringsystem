import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { MonitoringsystemEventModule } from './event/event.module';
import { MonitoringsystemEventTypeModule } from './event-type/event-type.module';
import {MonitoringsystemDayTimeModule} from './day-time/day-time.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    imports: [
        MonitoringsystemEventModule,
        MonitoringsystemEventTypeModule,
        MonitoringsystemDayTimeModule
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonitoringsystemEntityModule {}
