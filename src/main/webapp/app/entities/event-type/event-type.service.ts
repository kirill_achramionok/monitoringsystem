import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { SERVER_API_URL } from '../../app.constants';

import { EventType } from './event-type.model';
import { createRequestOption } from '../../shared';

export type EntityResponseType = HttpResponse<EventType>;

@Injectable()
export class EventTypeService {

    private resourceUrl =  SERVER_API_URL + 'api/event-types';

    constructor(private http: HttpClient) { }

    create(eventType: EventType): Observable<EntityResponseType> {
        const copy = this.convert(eventType);
        return this.http.post<EventType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    update(eventType: EventType): Observable<EntityResponseType> {
        const copy = this.convert(eventType);
        return this.http.put<EventType>(this.resourceUrl, copy, { observe: 'response' })
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    find(id: number): Observable<EntityResponseType> {
        return this.http.get<EventType>(`${this.resourceUrl}/${id}`, { observe: 'response'})
            .map((res: EntityResponseType) => this.convertResponse(res));
    }

    query(req?: any): Observable<HttpResponse<EventType[]>> {
        const options = createRequestOption(req);
        return this.http.get<EventType[]>(this.resourceUrl, { params: options, observe: 'response' })
            .map((res: HttpResponse<EventType[]>) => this.convertArrayResponse(res));
    }

    delete(id: number): Observable<HttpResponse<any>> {
        return this.http.delete<any>(`${this.resourceUrl}/${id}`, { observe: 'response'});
    }

    private convertResponse(res: EntityResponseType): EntityResponseType {
        const body: EventType = this.convertItemFromServer(res.body);
        return res.clone({body});
    }

    private convertArrayResponse(res: HttpResponse<EventType[]>): HttpResponse<EventType[]> {
        const jsonResponse: EventType[] = res.body;
        const body: EventType[] = [];
        for (let i = 0; i < jsonResponse.length; i++) {
            body.push(this.convertItemFromServer(jsonResponse[i]));
        }
        return res.clone({body});
    }

    /**
     * Convert a returned JSON object to EventType.
     */
    private convertItemFromServer(eventType: EventType): EventType {
        const copy: EventType = Object.assign({}, eventType);
        return copy;
    }

    /**
     * Convert a EventType to a JSON which can be sent to the server.
     */
    private convert(eventType: EventType): EventType {
        const copy: EventType = Object.assign({}, eventType);
        return copy;
    }
}
