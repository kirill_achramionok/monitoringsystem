import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { EventType } from './event-type.model';
import { EventTypeService } from './event-type.service';
import { Principal } from '../../shared';

@Component({
    selector: 'jhi-event-type',
    templateUrl: './event-type.component.html'
})
export class EventTypeComponent implements OnInit, OnDestroy {
eventTypes: EventType[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private eventTypeService: EventTypeService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {
    }

    loadAll() {
        this.eventTypeService.query().subscribe(
            (res: HttpResponse<EventType[]>) => {
                this.eventTypes = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }
    ngOnInit() {
        this.loadAll();
        this.principal.identity().then((account) => {
            this.currentAccount = account;
        });
        this.registerChangeInEventTypes();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: EventType) {
        return item.id;
    }
    registerChangeInEventTypes() {
        this.eventSubscriber = this.eventManager.subscribe('eventTypeListModification', (response) => this.loadAll());
    }

    private onError(error) {
        this.jhiAlertService.error(error.message, null, null);
    }
}
