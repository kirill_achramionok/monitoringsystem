import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { JhiEventManager } from 'ng-jhipster';

import { EventType } from './event-type.model';
import { EventTypeService } from './event-type.service';

@Component({
    selector: 'jhi-event-type-detail',
    templateUrl: './event-type-detail.component.html'
})
export class EventTypeDetailComponent implements OnInit, OnDestroy {

    eventType: EventType;
    private subscription: Subscription;
    private eventSubscriber: Subscription;

    constructor(
        private eventManager: JhiEventManager,
        private eventTypeService: EventTypeService,
        private route: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.subscription = this.route.params.subscribe((params) => {
            this.load(params['id']);
        });
        this.registerChangeInEventTypes();
    }

    load(id) {
        this.eventTypeService.find(id)
            .subscribe((eventTypeResponse: HttpResponse<EventType>) => {
                this.eventType = eventTypeResponse.body;
            });
    }
    previousState() {
        window.history.back();
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
        this.eventManager.destroy(this.eventSubscriber);
    }

    registerChangeInEventTypes() {
        this.eventSubscriber = this.eventManager.subscribe(
            'eventTypeListModification',
            (response) => this.load(this.eventType.id)
        );
    }
}
