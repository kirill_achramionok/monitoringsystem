import { Routes } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { EventTypeComponent } from './event-type.component';
import { EventTypeDetailComponent } from './event-type-detail.component';
import { EventTypePopupComponent } from './event-type-dialog.component';
import { EventTypeDeletePopupComponent } from './event-type-delete-dialog.component';

export const eventTypeRoute: Routes = [
    {
        path: 'event-type',
        component: EventTypeComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EventTypes'
        },
        canActivate: [UserRouteAccessService]
    }, {
        path: 'event-type/:id',
        component: EventTypeDetailComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EventTypes'
        },
        canActivate: [UserRouteAccessService]
    }
];

export const eventTypePopupRoute: Routes = [
    {
        path: 'event-type-new',
        component: EventTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EventTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-type/:id/edit',
        component: EventTypePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EventTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    },
    {
        path: 'event-type/:id/delete',
        component: EventTypeDeletePopupComponent,
        data: {
            authorities: ['ROLE_USER'],
            pageTitle: 'EventTypes'
        },
        canActivate: [UserRouteAccessService],
        outlet: 'popup'
    }
];
