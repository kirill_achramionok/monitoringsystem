import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { MonitoringsystemSharedModule } from '../../shared';
import {
    EventTypeService,
    EventTypePopupService,
    EventTypeComponent,
    EventTypeDetailComponent,
    EventTypeDialogComponent,
    EventTypePopupComponent,
    EventTypeDeletePopupComponent,
    EventTypeDeleteDialogComponent,
    eventTypeRoute,
    eventTypePopupRoute,
} from './';

const ENTITY_STATES = [
    ...eventTypeRoute,
    ...eventTypePopupRoute,
];

@NgModule({
    imports: [
        MonitoringsystemSharedModule,
        RouterModule.forChild(ENTITY_STATES)
    ],
    declarations: [
        EventTypeComponent,
        EventTypeDetailComponent,
        EventTypeDialogComponent,
        EventTypeDeleteDialogComponent,
        EventTypePopupComponent,
        EventTypeDeletePopupComponent,
    ],
    entryComponents: [
        EventTypeComponent,
        EventTypeDialogComponent,
        EventTypePopupComponent,
        EventTypeDeleteDialogComponent,
        EventTypeDeletePopupComponent,
    ],
    providers: [
        EventTypeService,
        EventTypePopupService,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class MonitoringsystemEventTypeModule {}
