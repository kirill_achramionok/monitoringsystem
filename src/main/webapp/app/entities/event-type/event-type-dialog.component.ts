import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { EventType } from './event-type.model';
import { EventTypePopupService } from './event-type-popup.service';
import { EventTypeService } from './event-type.service';

@Component({
    selector: 'jhi-event-type-dialog',
    templateUrl: './event-type-dialog.component.html'
})
export class EventTypeDialogComponent implements OnInit {

    eventType: EventType;
    isSaving: boolean;

    constructor(
        public activeModal: NgbActiveModal,
        private eventTypeService: EventTypeService,
        private eventManager: JhiEventManager
    ) {
    }

    ngOnInit() {
        this.isSaving = false;
    }

    clear() {
        this.activeModal.dismiss('cancel');
    }

    save() {
        this.isSaving = true;
        if (this.eventType.id !== undefined) {
            this.subscribeToSaveResponse(
                this.eventTypeService.update(this.eventType));
        } else {
            this.subscribeToSaveResponse(
                this.eventTypeService.create(this.eventType));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<EventType>>) {
        result.subscribe((res: HttpResponse<EventType>) =>
            this.onSaveSuccess(res.body), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess(result: EventType) {
        this.eventManager.broadcast({ name: 'eventTypeListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError() {
        this.isSaving = false;
    }
}

@Component({
    selector: 'jhi-event-type-popup',
    template: ''
})
export class EventTypePopupComponent implements OnInit, OnDestroy {

    routeSub: any;

    constructor(
        private route: ActivatedRoute,
        private eventTypePopupService: EventTypePopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe((params) => {
            if ( params['id'] ) {
                this.eventTypePopupService
                    .open(EventTypeDialogComponent as Component, params['id']);
            } else {
                this.eventTypePopupService
                    .open(EventTypeDialogComponent as Component);
            }
        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
